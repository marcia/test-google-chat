# Test Google Chats

Test Google Chats integration for TW OKR: https://gitlab.com/gitlab-org/gitlab/-/issues/300599

- GitLab Docs: https://docs.gitlab.com/ee/user/project/integrations/hangouts_chat.html#hangouts-chat-service
- Google Chat docs:
    - New webhook with Python: https://developers.google.com/hangouts/chat/quickstart/incoming-bot-python\
- Google's doc is misleading as it indicates that we need Python3, pip, and a script `quickstart.py` to make the connection work. But in the scope of GitLab, we don't need that, we only need the webhook URL.

Hello world!

## How to integrate Google Chat with GitLab

In Google Chat:

1. Enter the room where you want to receive notifications from GitLab.
1. Open the room dropdown menu on the top-left and select **Manage webhooks**.
1. Enter the name for your webhook, for example "GitLab integration".
1. (Optional) Add an avatar for your bot.
1. Select **Save**.
1. Copy the webhook URL.

In GitLab:

1. In your project, go to **Settings > Integrations** and select **Hangouts Chat**.
1. Enter the webhook URL you copied from the previous step.
1. (Optional) Select **Test settings** to verify the connection.
1. Select **Save changes**.

To test the integration, push a change to a file in your project and see the notification in your room in Google Chat.
